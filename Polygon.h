//
// Created by mgrus on 29/07/2021.
//

#ifndef MYCAD_POLYGON_H
#define MYCAD_POLYGON_H


#include "Vertex.h"
#include <vector>

class PolygonObj {

public:
    PolygonObj(std::vector<Vertex*> initialVertices, bool closed);
    std::vector<Vertex*> getVertices() const;
    bool isClosed() const;

private:
    std::vector<Vertex*> _vertices;
    bool _closed;
};


#endif //MYCAD_POLYGON_H
