//
// Created by mgrus on 29/07/2021.
//

#ifndef MYCAD_VERTEX_H
#define MYCAD_VERTEX_H


class Vertex {
public:
    float x;
    float y;
    float z;
};


#endif //MYCAD_VERTEX_H
