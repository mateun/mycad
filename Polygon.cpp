//
// Created by mgrus on 29/07/2021.
//

#include "Polygon.h"

PolygonObj::PolygonObj(std::vector<Vertex*> initialVertices, bool closed) : _closed(closed) {
    for (auto v : initialVertices) {
        _vertices.push_back(v);
    }

}

std::vector<Vertex*> PolygonObj::getVertices() const {
    return _vertices;
}

bool PolygonObj::isClosed() const
{
    return _closed;
}