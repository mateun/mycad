//
// Created by mgrus on 29/07/2021.
//

#include "PolygonRenderer.h"
#include <stdio.h>

#define PI  3.14159

PolygonRenderer::PolygonRenderer(HWND hwnd, int screenWidth, int screenHeight) : _hwnd(hwnd), _screenWidth (screenWidth), _screenHeight(screenHeight) {

}

void PolygonRenderer::renderPolygon(const PolygonObj* polygon) {
    renderPolygon(polygon, 0, 0, 0, 0);
}

void PolygonRenderer::renderPolygon(const PolygonObj *polygon, double rotY, double camX, double camY, double camZ) {
    HDC hdc = GetDC(_hwnd);
    RECT rect;
    GetClientRect (_hwnd, &rect) ;
    
    // First, camera transform
    // Reverse move and rotate as if the camera is in the center
    // of the coordinate system.
    HBRUSH brush = CreateSolidBrush(RGB(250, 20, 30));
    std::vector<Vertex> screenVertices;
    for (auto v : polygon->getVertices()) {


        // Modeling movement
        // Using the cam movement here
        Vertex vModel;
        /*vModel.x = v->x;
        vModel.y = v->y;
        vModel.z = v->z + camZ;*/

        vModel.x = v->x;
        vModel.y = v->y;
        vModel.z = v->z;

      
        Vertex* vView = new Vertex();

        // Translation
        vView->x = vModel.x - camX;
        vView->y = vModel.y - camY;
        vView->z = vModel.z - camZ;

        


        double angle = -rotY;
        //angle = PI / 2;
        double xRot = vView->x * cos(angle) + vView->z * sin(angle);
        vView->y = vView->y;
        double mysin = sin(angle);
        double mycos = cos(angle);
        double zRot =  -(vView->x * mysin) + (vView->z * mycos);
        vView->x = xRot;
        vView->z = zRot;

        char bf[100];
        sprintf_s(bf, 100, "camZ: %f v.z:%f view.z:%f\n", camZ, v->z, vView->z);
        OutputDebugString(bf);

        char buf[50];
        sprintf_s(buf, 50, "z: %f rot: %f\n\n", (float) vView->z, angle);
        OutputDebugString(buf);

        DrawText(hdc, buf,
            -1, &rect, DT_SINGLELINE | DT_RIGHT | DT_TOP);
        
        // Rotation around Y axis
        //double angle =rotY;
        //vView->x = vView->x * cos(angle) + vView->z * sin(angle);
        //vView->y = vView->y;
        //vView->z = -vView->x * sin(angle) + vView->z * cos(angle);


        // Project x and y to the view plane.
        
        // We transform into clip space
        // which is a transform from camera space into a 2 unit cuboid.
        double ar = (double)600 / 800;
        double fov = 60;
        double fovY = 47;
        double zFar = 50;
        double zNear = .01;
        Vertex vCS;
        vCS.x = vView->x / tan(fov / 2);
        vCS.y = vView->y / tan(fov / 2);
        vCS.z = vView->z;
        
        // TODO clipping

        double a = tan(PI/4);
        Vertex vProj;
        vProj.x = vView->x / vView->z;
        vProj.y = vView->y / vView->z;
        vProj.z = vView->z;


        if (vView->z > zNear) {
          
        
            /*&& vView->z < zFar*/
            //) {
            // Transform to screen coordinates.
            // Going from -1 to 1 to screen coordinates.
            Vertex vScreen;
            int screenW = rect.right - rect.left;
            int screenH = rect.bottom - rect.top;
            vScreen.x = vProj.x * (screenW / 2) + (screenW / 2);
            vScreen.y = screenH - (vProj.y * (screenH / 2) + (screenH / 2));



            // Clip 
           /* if (vScreen.x < 0 || vScreen.x > screenW ||
                vScreen.y < 0 || vScreen.y > screenH) {

                screenVertices.push_back(vScreen);
            }
            else {
                screenVertices.push_back(vScreen);
            }*/


            screenVertices.push_back(vScreen);


            RECT r;
            r.left = vScreen.x - 2;
            r.right = vScreen.x + 2;
            r.bottom = vScreen.y - 2;
            r.top = vScreen.y + 2;

            FillRect(hdc, &r, brush);
        }

        

        delete(vView);
    }
   

    HGDIOBJ original = SelectObject(hdc, GetStockObject(DC_PEN));
    SelectObject(hdc, GetStockObject(DC_PEN));
    SetDCPenColor(hdc, RGB(150, 180, 240));

    int counter = 0;
    for (auto vs : screenVertices) {

        if (counter > 0 && counter < screenVertices.size()) {
            Vertex vStart = screenVertices[((LONGLONG)counter)-1];
            Vertex vEnd = screenVertices[counter];
            MoveToEx(hdc, vStart.x, vStart.y, nullptr);
            LineTo(hdc, vEnd.x, vEnd.y);
        }

        if (polygon->isClosed()) {
            if (counter == screenVertices.size() - 1) {
                // Draw a line from last to first vertex.
                Vertex vStart = screenVertices[counter];
                Vertex vEnd = screenVertices[0];
                MoveToEx(hdc, vStart.x, vStart.y, nullptr);
                LineTo(hdc, vEnd.x, vEnd.y);
            }
        }
        

        counter++;
    }

    DeleteObject(brush);
    ReleaseDC(_hwnd, hdc);

}
