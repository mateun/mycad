#include <iostream>
#include <Windows.h>
#include "Polygon.h"
#include "PolygonRenderer.h"
#include "Vertex.h"
#include <vector>

std::vector<class PolygonObj*> g_polygons;
double rotY = 0;
double camX = 0;
double camY =1.5;
double camZ = -8;

void initPolys() {
    if (g_polygons.size() == 0) {
        std::vector<Vertex*> verticesP1;
        verticesP1.push_back(new Vertex{ -0.5, -0.5, 0 });
        verticesP1.push_back(new Vertex{ 0.5, -0.5, 0 });
        verticesP1.push_back(new Vertex{ 0.5, 0.5, 0 });
        verticesP1.push_back(new Vertex{ -0.5, 0.5, 0 });
        PolygonObj* p1 = new PolygonObj(verticesP1, true);
        g_polygons.push_back(p1);


        std::vector<Vertex*> verticesPEast;
        verticesPEast.push_back(new Vertex{ 3.5, 0.5, -3 });
        verticesPEast.push_back(new Vertex{ 3.5, -0.5, -2 });
        verticesPEast.push_back(new Vertex{ 3.5, 0.5, -3 });
        verticesPEast.push_back(new Vertex{ 3.5, 0.5, -2 });
        PolygonObj* pEast = new PolygonObj(verticesPEast, true);
        //g_polygons.push_back(pEast);



        std::vector<Vertex*> verticesP2;
        verticesP2.push_back(new Vertex{ -0.5, -0.5, 1 });
        verticesP2.push_back(new Vertex{ 0.5, -0.5, 1 });
        verticesP2.push_back(new Vertex{ 0.5, 0.5, 1 });
        verticesP2.push_back(new Vertex{ -0.5, 0.5, 1 });
        PolygonObj* p2 = new PolygonObj(verticesP2, true);
        g_polygons.push_back(p2);

        std::vector<Vertex*> verticesP3;
        verticesP3.push_back(new Vertex{ -0.5, -0.5, 0 });
        verticesP3.push_back(new Vertex{ -0.5, -0.5, 1 });
        verticesP3.push_back(new Vertex{ 0.5, -0.5, 0 });
        verticesP3.push_back(new Vertex{ 0.5, -0.5, 1 });
        PolygonObj* p3 = new PolygonObj(verticesP3, true);
        g_polygons.push_back(p3);


        for (int i = 0; i < 25; i++) {
            Vertex* vStart = new Vertex{ -5 + (float)i, -1.5, -3};
            Vertex* vEnd = new Vertex{ -5 + (float) i, -1.5, 26};
            std::vector<Vertex*> vs;
            vs.push_back(vStart);
            vs.push_back(vEnd);
            PolygonObj* p = new PolygonObj(vs, false);
            g_polygons.push_back(p);
        }


        for (int i = 0; i < 45; i++) {
            Vertex* vStart = new Vertex{ -20, -1.5f, -13.5f + (float)i };
            Vertex* vEnd = new Vertex{ 20, -1.5f, -13.5f + (float)i };
            std::vector<Vertex*> vs;
            vs.push_back(vStart);
            vs.push_back(vEnd);
            PolygonObj* p = new PolygonObj(vs, false);
            g_polygons.push_back(p);
        }

    }
}

void renderPolys(HWND hwnd) {
    PolygonRenderer renderer(hwnd, 800, 600);
    for (auto p : g_polygons) {
        renderer.renderPolygon(p, rotY, camX, camY, camZ);
    }
}

void fillBackground(HWND hwnd, int r, int g, int b) {
    RECT rect;
    GetClientRect(hwnd, &rect);
    HBRUSH br = CreateSolidBrush(RGB(r, g, b));
    HDC hdc = GetDC(hwnd);
    FillRect(hdc, &rect, br);
    DeleteObject(br);
}

void renderCrosshair(HDC hdc, RECT& rect) {

    HGDIOBJ original = SelectObject(hdc, GetStockObject(DC_PEN));
    SelectObject(hdc, GetStockObject(DC_PEN));
    SetDCPenColor(hdc, RGB(250, 180, 240));

    // Render crosshair
    int startX = ((rect.right - rect.left) / 2) - 2;
    int startY = ((rect.bottom - rect.top) / 2) - 2;
    MoveToEx(hdc, startX, startY, nullptr);
    LineTo(hdc, startX + 2, startY + 2);

   
}


LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{

    HDC hdc;
    RECT rect;
    TEXTMETRIC tm;
    PAINTSTRUCT ps;

    switch (message) {

        case WM_KEYDOWN:
            switch (wParam)
            {
            case VK_LEFT:
                rotY -= 0.01;
                break;

            case VK_RIGHT:
                rotY += 0.01;
                break;

            case VK_ADD:
                camZ += 0.01;
                break;
            case VK_SUBTRACT:
                camZ -= 0.01;
                break;

            case VK_NUMPAD4:
                camX += 0.01;
                break;

            case VK_NUMPAD6:
                camX -= 0.01;
                break;

            case VK_RETURN:
                camX = 0;
                camY = 0;
                camZ = 6;
                rotY = 0;
                break;
            }
            RedrawWindow(hwnd, NULL, NULL, RDW_INVALIDATE | RDW_UPDATENOW);
            break;

        case WM_CLOSE:
           /* if (MessageBox(NULL, "Are you sure you want to quit?",
                           "Confirmation", MB_ICONQUESTION | MB_YESNO) == IDYES)*/
            DestroyWindow(hwnd);
            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;

        case WM_PAINT: {

            hdc = BeginPaint(hwnd, &ps);
            fillBackground(hwnd, 10, 10, 15);

            GetClientRect(hwnd, &rect);
            DrawText(hdc, TEXT ("Viewport: 3D"),
                     -1, &rect, DT_SINGLELINE | DT_LEFT | DT_TOP);
            
            initPolys();
            renderPolys(hwnd);

            renderCrosshair(hdc, rect);

            EndPaint(hwnd, &ps);

            break;
        }
        default:
            return DefWindowProc(hwnd, message, wParam, lParam);
    }
    return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
            LPSTR lpCmdLine, int nCmdShow)
{
    const char g_szClassName[] = "bonafideideasWindowClass";
    WNDCLASSEX wc;

    wc.lpszMenuName  = NULL;
    wc.hInstance     = hInstance;
    wc.lpszClassName = g_szClassName;
    wc.cbSize        = sizeof(WNDCLASSEX);
    wc.cbClsExtra      = wc.cbWndExtra  = 0;
    wc.style         = CS_HREDRAW | CS_VREDRAW ;
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
    wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
    wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
    wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);
    wc.lpfnWndProc = WndProc;

    if(!RegisterClassEx(&wc))    {
        MessageBox(NULL, "Window Registration Failed", "Error",   MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    HWND hwnd = CreateWindowEx(
            WS_EX_CLIENTEDGE,
            g_szClassName,
            "Simplest Windows Native App built with CLion",
            WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT, CW_USEDEFAULT, 800, 600,
            NULL, NULL, hInstance, NULL);

    if(hwnd == NULL)    {
        MessageBox(NULL, "Window Creation Failed", "Error",   MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    
   

    MSG msg;
    while (GetMessage(&msg, nullptr, 0, 0) > 0)
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return (int) msg.wParam;
}



