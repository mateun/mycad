//
// Created by mgrus on 29/07/2021.
//

#ifndef MYCAD_POLYGONRENDERER_H
#define MYCAD_POLYGONRENDERER_H

#include <Windows.h>
#include "Polygon.h"



class PolygonRenderer {

public:
    PolygonRenderer(HWND hwnd, int screenWidth, int screenHeight);
    void renderPolygon(const class PolygonObj* polygon);
    void renderPolygon(const class PolygonObj* polygon, double rotY, double camX, double camY, double camZ);

private:
    HWND _hwnd;
    int _screenWidth;
    int _screenHeight;
};


#endif //MYCAD_POLYGONRENDERER_H
